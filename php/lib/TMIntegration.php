<?php

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use GuzzleHttp\Client;

class TMIntegration {
  const PRIVATE_KEY = <<<PRK
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEApssOfmN1jRVFCbunlOJP9VDBxFa3AT7L8mnzJoemo9PAIu1e
s3EvmqTkfheT+Mk1fnMCjyb/8gUpeSzXo0Ebq0ZErOUAAOIv1K38K4k0WGQ3po3E
habv9Ri9CKGEwbwILpmq7DSwAgfQNMnTo0fjXTyIRXJqRfsMCylhVWRJd0KNXjIZ
WT3SYN889L6HmWlZ9g8dfvJFFvjXcX0XW4clD+ePH2lEs7oJdM0EYxOmxXTNKDUi
EdfI5FBCSgJuRcVK9F1afXEpJIuiKmDmC5BdBZdLs71wBCUMjrwRSxh5uhIpDS3j
po0qqwqM5DEo+jcJm3oqzR3FoWGLElwfZ1fphwIDAQABAoIBACUZ8gVnNlTFsG+z
0Fkr5kFZT2DPMTITt+Yabz3Y2taCWbDrQC+GM+KgXzs1H3Mtvrp79svFDxXU1B7i
PK1R5Ee6/7e90Lxhk+YpNXixsnUfBcoEqvxX3/STJilvVEqL+v3yrLSSG7y35DZ5
1pvxd7/boSxhgQZEC6/H6qDGCBmA1mWX69syXiVUKZ2podQwZOqVnhnTX6PCBzaY
K6tKxgrdLHpq97/VOn8L+Or5IxaYZSC3CRz06aE1B2BI51A69EqzlWmn4NGzhzGH
xkLGzoMCIfV4+zRw/5E9077mhWYTVi8Iv1aOB+4I+zw0qzt2wJVfcZcZDlShUxE0
4hwF38ECgYEA0Sncy5ks60X3UMCvSPFjx1Vp7GNCHtdFimM+xQFRPrORrehyV6nF
q8vg4CUI5Q1XisTntnDll9qjh14/X51yJ7Z/3K1IgKTkKkKXpAtz4X9RUr54q4Pd
fCQHX+3GKqaKPNxicWtoaW8H4xyvX6BQCF0WglOr8/2Ixvhm6zcm6lECgYEAzCRY
7k5EyeOwjgLDJURF5VI+sgx0YdR7BPm+TKwSH3B5Sy7ld7gjvSZwKeFbCw6ymbci
ZlNTyJ6l8QUdXIlyxdwf3nILvrPoo1T/LEAnavNQYvYhUXQWvsXbRiBGm7NkBiOJ
n0TZIUFVNhqJIwOKAKT6fNgV4Y43z1S3ge5fyFcCgYBwv08clKoI/MMOzX7vI+e0
KI+bc/nNZB9BNLQ+P7KKyQMLKpSQOpyqOZEzYeNEjQeWbwO0bXDlhV7G9n/Uy6sg
SxjrXKELb1bqjNDvBZlmgVnV74ReeAtpAobTJ/m9Pd6gZe/O0aYLx2UKAIlsU58w
kvUyNWYodI9JKddYjXc1sQKBgB2BI1lN9qM3RU1BGPNJ6tA6EtWo9yksjJsrfVWX
iQcCOoWjQYcMFh5Ab/WdnIr8lNOipFYltPZJ97HeC2BUfUIbjn+X/BqT5gu1PSwo
GYpboEKtUCN1o9PuxMJd59aZFtvMRxe926PyzV4Ed82XtVpifdGNaZwv8eEFXYBp
824BAoGBAIQYjhoVy1avJlX3OmlOQK9BY4m1eWQsYFuzNlUtpWjh+w5+Syz4R/jH
FvW+BW/Y/+WMV+DQYlH6Odwcqr/RlykzjZ+t8RY3V91zxer2GhUh2HlF1wF0enza
xz0nWPtd+scs/AS3Udth+YPKX+rc1t4nQE/+5xSz9rpTTLK11Vdx
-----END RSA PRIVATE KEY-----
PRK;

  const PUBLIC_KEY = <<<PUK
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwC39bo8/zG2Xzsioi+pY
srdrlGx1Dmq/HR+v72Bs+Kn2/FNU69R69Jd+e0s+qIYdn1A8RZDl8R85G8o9zkUv
AlJYqeSLeSbyKhBgsBXBRKyH5PwC4eD9x4nnq64r1UF/FQpvp4nla1SqUDSTL8kU
6O/a8sYhWnXzD1qtpUNTyNoz0LqVIND92hXI3bE3SlpYIclvIzDGXkmYwD7StW8D
xJQQh+FvZ8Ft0unTaz20R0g9Nie0gSxH5+MB43uBU9Wp/PU/iBEkab6co9gLVs0h
iFSPi3Wi4rbg9BpEuMLS/u3QOIwdXsrBt5IQagAO+jLXS8Il4P/Hy6hyBaAA3t4J
6QIDAQAB
-----END PUBLIC KEY-----
PUK;

  const CLIENT_ID = '76BP2FCKRY0SIL9G2C6ILT0YA3AKI3WU';
  const CLIENT_SECRET = 'THJ3OH5SNNAPISVVGOEOMBBZMVZDVKI3N6KIPF7XAZ0VVTUZYWSHL8LSKYG7AN9N';

  const USERNAME = '230510008';
  const PASSWORD = 'YbfRkGiiZR0ifhvZzdT4jVM2vf8xAfwnSeatwu05GFhF6/xi5qya/p2Xx+6oPiAFV2jXm21w9+9jsajRhDmY+prFFVx3m3qsR1jkJ75qVW3B3FmE+o3uI6hVrnCpXKRQLw7wBFICd/RgJFRjB2fxVC32UFczoyQIPvlzEbwrjxs=';

  const API_BASE_URI = 'https://local-channel-gateway-qa.dev.truemoney.com.kh';

  private $httpClient;
  private $tokenData;
  private $tokenExpiresAt;

  public function __construct() {
    $this->httpClient = $this->getHttpClient();
  }

  private function getHttpClient() {
    return new Client([
      'verify' => false,
      'base_uri' => self::API_BASE_URI,
      'headers' => [
        'client_id' => self::CLIENT_ID,
        'client_secret' => self::CLIENT_SECRET,
      ]
    ]);
  } 

  private function getAuthorizationKey (): string {
    return base64_encode(self::CLIENT_ID . ':' . self::CLIENT_SECRET);
  }
  
  private function jwtEncode(array $payload): string {
    return JWT::encode($payload, self::PRIVATE_KEY, 'RS512');
  }
  
  private function jwtDecode(string $jwt) {
    return JWT::decode($jwt, new Key(self::PUBLIC_KEY, 'RS512'));
  }

  private function handleApiResponse($response): array {
    $responseContent = json_decode($response->getBody()->getContents(), true);

    if($responseContent['status']['code'] !== 'success') {
      throw new \UnexpectedValueException($resultContent['status']['message']);
    }

    return $responseContent;
  }

  public function getToken(): array {
    if(isset($this->tokenData) && !$this->isTokenExpiredInSeconds()) {
      return $this->tokenData;
    }
    else {
      $tokenData = $this->login();
  
      $this->setTokenExpiresAt($tokenData['access_token_expires_in']);
      $this->tokenData = $tokenData;
  
      return $tokenData;
    }
  }

  public function checkTransaction($token, array $payload): array {
    $response = $this->httpClient->post('/partner/local-channel-adapter/batch/transaction/status', [
      'headers' => [
        'Content-Type' => 'text/plain',
        'Authorization' => "Bearer $token",
      ],
      'body' => $this->jwtEncode($payload)
    ]);
    $content = $response->getBody()->getContents();
    return json_decode(json_encode($this->jwtDecode($content)), true);
  }

  private function login(): array {
    $response = $this->httpClient->post('/local-channel-adapter/oauth/exchange/agent', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => "Basic {$this->getAuthorizationKey()}",
      ],
      'form_params' => [
        'grant_type' => 'password',
        'client_id' => self::CLIENT_ID,
        'username' => self::USERNAME,
        'password' => self::PASSWORD
      ]
    ]);

    $content = $this->handleApiResponse($response);
    return $content['data'];
  }

  private function setTokenExpiresAt(int $expiresInSeconds): void {
    $this->tokenExpiresAt = (new \DateTime())->modify("+ $expiresInSeconds second")->format('Y-m-d H:i:s');
  }

  private function isTokenExpiredInSeconds(int $inSeconds = 60): bool {
    $current = strtotime(date('Y-m-d H:i:s'));
    $expiresAt = strtotime($this->tokenExpiresAt);
    $remainingSecondsTillTokenExpires = $expiresAt - $current;
    return $remainingSecondsTillTokenExpires <= $inSeconds;
  }
}
