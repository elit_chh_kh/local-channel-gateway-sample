<?php
require 'vendor/autoload.php';

$tmIntegration = new TMIntegration();

$token = $tmIntegration->getToken();

$payload = [
  'iat' => time(),
  'orderIds' => ['220113064715095774024031'],
  // 'refNums' => ['233454587','233454588']
];
$resultCheckTxn = $tmIntegration->checkTransaction($token['access_token'], $payload);

echo json_encode($resultCheckTxn, JSON_PRETTY_PRINT);